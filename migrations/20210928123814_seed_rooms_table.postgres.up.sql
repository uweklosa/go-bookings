INSERT INTO "rooms" ("id", "room_name", "created_at", "updated_at")
VALUES (1, 'General''s Quarters', now(), now()),
       (2, 'Major''s Suite', now(), now());