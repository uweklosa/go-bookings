package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/uweklosa/bookings/internal/models"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"strings"
	"testing"
	"time"
)

var theTests = []struct {
	name               string
	url                string
	method             string
	expectedStatusCode int
}{
	{"home", "/", "GET", http.StatusOK},
	{"about", "/about", "GET", http.StatusOK},
	{"gq", "/generals-quarters", "GET", http.StatusOK},
	{"ms", "/majors-suite", "GET", http.StatusOK},
	{"sa", "/search-availability", "GET", http.StatusOK},
	{"contact", "/contact", "GET", http.StatusOK},
	{"rs", "/reservation-summary", "GET", http.StatusOK},
	{"non-existent", "/non-existent", "GET", http.StatusNotFound},
	{"login", "/user/login", "GET", http.StatusOK},
	{"logout", "/user/logout", "GET", http.StatusOK},
	{"dashboard", "/admin/dashboard", "GET", http.StatusOK},
	{"new reservations", "/admin/reservations-new", "GET", http.StatusOK},
	{"all reservations", "/admin/reservations-all", "GET", http.StatusOK},
	{"show reservation", "/admin/reservations/new/1/show", "GET", http.StatusOK},
	{"show reservation calendar", "/admin/reservations-calendar", "GET", http.StatusOK},
	{"show reservation calendar with params", "/admin/reservations-calendar?y=2020&m=1", "GET", http.StatusOK},
}

func TestHandlers(t *testing.T) {
	routes := getRoutes()
	ts := httptest.NewTLSServer(routes)
	defer ts.Close()

	for _, e := range theTests {
		resp, err := ts.Client().Get(ts.URL + e.url)
		if err != nil {
			t.Log(err)
			t.Fatal(err)
		}

		if resp.StatusCode != e.expectedStatusCode {
			t.Errorf("for %s, expected %d but got %d", e.name, e.expectedStatusCode, resp.StatusCode)
		}
	}
}

func TestRepository_Reservation(t *testing.T) {
	tests := []struct {
		name             string
		reservation      models.Reservation
		storeReservation bool
		want             int
	}{
		{"valid request",
			models.Reservation{
				RoomID: 1,
				Room: models.Room{
					ID:       1,
					RoomName: "General's Quarters",
				},
			},
			true,
			http.StatusOK},
		{"invalid request", models.Reservation{}, false, http.StatusSeeOther},
		{"invalid room id",
			models.Reservation{
				RoomID: 10,
			},
			true,
			http.StatusSeeOther},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, _ := http.NewRequest("GET", "/make-reservation", nil)
			ctx := getCtx(req)
			req = req.WithContext(ctx)

			rr := httptest.NewRecorder()
			if tt.storeReservation {
				session.Put(ctx, reservationKey, tt.reservation)
			}

			handler := http.HandlerFunc(Repo.Reservation)

			handler.ServeHTTP(rr, req)
			if rr.Code != tt.want {
				t.Errorf("ReservationSummary handler returned wrong response code: got %d, wanted %d", rr.Code, tt.want)
			}
		})
	}
}

func getCtx(req *http.Request) context.Context {
	ctx, err := session.Load(req.Context(), req.Header.Get("X-Session"))
	if err != nil {
		log.Println(err)
	}
	return ctx
}

func TestRepository_PostReservation(t *testing.T) {
	type Fields struct {
		StartDate string
		EndDate   string
		FirstName string
		LastName  string
		Email     string
		Phone     string
		RoomId    string
	}
	tests := []struct {
		name     string
		fields   Fields
		withBody bool
		want     int
	}{
		{"invalid request",
			Fields{},
			false,
			http.StatusSeeOther},
		{"valid request",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				FirstName: "John",
				LastName:  "Smith",
				Email:     "john@smith.com",
				Phone:     "123456789",
				RoomId:    "1",
			},
			true,
			http.StatusSeeOther},
		{"invalid start date",
			Fields{
				StartDate: "invalid",
				EndDate:   "2050-01-02",
				FirstName: "John",
				LastName:  "Smith",
				Email:     "john@smith.com",
				Phone:     "123456789",
				RoomId:    "1",
			},
			true,
			http.StatusSeeOther},
		{"invalid end date",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "invalid",
				FirstName: "John",
				LastName:  "Smith",
				Email:     "john@smith.com",
				Phone:     "123456789",
				RoomId:    "1",
			},
			true,
			http.StatusSeeOther},
		{"invalid room id",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				FirstName: "John",
				LastName:  "Smith",
				Email:     "john@smith.com",
				Phone:     "123456789",
				RoomId:    "invalid",
			},
			true,
			http.StatusSeeOther},
		{"non existing room id",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				FirstName: "John",
				LastName:  "Smith",
				Email:     "john@smith.com",
				Phone:     "123456789",
				RoomId:    "10",
			},
			true,
			http.StatusSeeOther},
		{"too short firstname",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				FirstName: "J",
				LastName:  "Smith",
				Email:     "john@smith.com",
				Phone:     "123456789",
				RoomId:    "1",
			},
			true,
			http.StatusOK},
		{"invalid email",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				FirstName: "John",
				LastName:  "Smith",
				Email:     "johnsmith.com",
				Phone:     "123456789",
				RoomId:    "1",
			},
			true,
			http.StatusOK},
		{"reservation insert fails",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				FirstName: "John",
				LastName:  "Smith",
				Email:     "john@smith.com",
				Phone:     "123456789",
				RoomId:    "100",
			},
			true,
			http.StatusSeeOther},
		{"room restriction insert fails",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				FirstName: "John",
				LastName:  "Smith",
				Email:     "john@smith.com",
				Phone:     "123456789",
				RoomId:    "1000",
			},
			true,
			http.StatusSeeOther},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			values := url.Values{}
			values.Add("start_date", tt.fields.StartDate)
			values.Add("end_date", tt.fields.EndDate)
			values.Add("first_name", tt.fields.FirstName)
			values.Add("last_name", tt.fields.LastName)
			values.Add("email", tt.fields.Email)
			values.Add("phone", tt.fields.Phone)
			values.Add("room_id", tt.fields.RoomId)

			var req *http.Request
			if tt.withBody {
				req, _ = http.NewRequest("POST", "/make-reservation", strings.NewReader(values.Encode()))
			} else {
				req, _ = http.NewRequest("POST", "/make-reservation", nil)
			}
			ctx := getCtx(req)
			req = req.WithContext(ctx)

			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

			rr := httptest.NewRecorder()

			handler := http.HandlerFunc(Repo.PostReservation)
			handler.ServeHTTP(rr, req)
			if rr.Code != tt.want {
				t.Errorf("%s returned wrong response code: got %d, wanted %d", tt.name, rr.Code, tt.want)
			}
		})
	}
}

func TestRepository_AvailabilityJSON(t *testing.T) {
	type Fields struct {
		StartDate string
		EndDate   string
		RoomId    string
	}
	tests := []struct {
		name     string
		fields   Fields
		withBody bool
		want     jsonResponse
	}{
		{"invalid request",
			Fields{},
			false,
			jsonResponse{
				Ok:      false,
				Message: "Internal server error",
			},
		},
		{"valid request",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				RoomId:    "1",
			},
			true,
			jsonResponse{
				Ok:        true,
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				RoomID:    "1",
			},
		},
		{"room not available",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				RoomId:    "2",
			},
			true,
			jsonResponse{
				Ok:        false,
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
				RoomID:    "2",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			values := url.Values{}
			values.Add("start", tt.fields.StartDate)
			values.Add("end", tt.fields.EndDate)
			values.Add("room_id", tt.fields.RoomId)

			var req *http.Request
			if tt.withBody {
				req, _ = http.NewRequest("POST", "/search-availability-json", strings.NewReader(values.Encode()))
			} else {
				req, _ = http.NewRequest("POST", "/search-availability-json", nil)
			}
			ctx := getCtx(req)
			req = req.WithContext(ctx)

			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

			rr := httptest.NewRecorder()

			handler := http.HandlerFunc(Repo.AvailabilityJSON)
			handler.ServeHTTP(rr, req)
			var j jsonResponse
			err := json.Unmarshal([]byte(rr.Body.String()), &j)
			if err != nil {
				t.Errorf("failed to parse json")
			}
			if !reflect.DeepEqual(j, tt.want) {
				t.Errorf("%s returned incorrect JSON response got: %+v want: %+v", tt.name, j, tt.want)
			}
		})
	}

}

func TestRepository_ChooseRoom(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}
	tests := []struct {
		name   string
		url    string
		roomID int
		want   int
	}{
		{"missing id", "/choose-room/invalid", 0, http.StatusSeeOther},
		{"missing reservation", "/choose-room/1", 0, http.StatusSeeOther},
		{"valid request", "/choose-room/1", 1, http.StatusSeeOther},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, _ := http.NewRequest("GET", tt.url, nil)
			ctx := getCtx(req)
			req = req.WithContext(ctx)
			req.RequestURI = tt.url

			rr := httptest.NewRecorder()
			if tt.roomID > 0 {
				session.Put(ctx, reservationKey, models.Reservation{RoomID: tt.roomID})
			}
			handler := http.HandlerFunc(Repo.ChooseRoom)
			handler.ServeHTTP(rr, req)
			if rr.Code != tt.want {
				t.Errorf("ChooseRoom handler returned wrong response code: got %d, wanted %d", rr.Code, tt.want)
			}
		})
	}
}

func TestRepository_BookRoom(t *testing.T) {
	tests := []struct {
		name string
		url  string
		want int
	}{
		{"missing all parameters", "/book-room", http.StatusSeeOther},
		{"missing dates", "/book-room?id=1", http.StatusSeeOther},
		{"missing end date", "/book-room?id=1&s=2050-01-01", http.StatusSeeOther},
		{"invalid room id", "/book-room?id=invalid&s=2050-01-01&e=2050-01-02", http.StatusSeeOther},
		{"non existing room id", "/book-room?id=10&s=2050-01-01&e=2050-01-02", http.StatusSeeOther},
		{"valid request", "/book-room?id=1&s=2050-01-01&e=2050-01-02", http.StatusSeeOther},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, _ := http.NewRequest("GET", tt.url, nil)
			ctx := getCtx(req)
			req = req.WithContext(ctx)

			rr := httptest.NewRecorder()

			handler := http.HandlerFunc(Repo.BookRoom)
			handler.ServeHTTP(rr, req)
			if rr.Code != tt.want {
				t.Errorf("BookRoom handler returned wrong response code: got %d, wanted %d", rr.Code, tt.want)
			}
		})
	}
}

func TestRepository_ReservationSummary(t *testing.T) {
	tests := []struct {
		name             string
		reservation      models.Reservation
		storeReservation bool
		want             int
	}{
		{"missing reservation", models.Reservation{}, false, http.StatusSeeOther},
		{"valid request", models.Reservation{}, true, http.StatusOK},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, _ := http.NewRequest("GET", "/reservation-summary", nil)
			ctx := getCtx(req)
			req = req.WithContext(ctx)

			rr := httptest.NewRecorder()
			if tt.storeReservation {
				session.Put(ctx, reservationKey, tt.reservation)
			}

			handler := http.HandlerFunc(Repo.ReservationSummary)

			handler.ServeHTTP(rr, req)
			if rr.Code != tt.want {
				t.Errorf("ReservationSummary handler returned wrong response code: got %d, wanted %d", rr.Code, tt.want)
			}
		})
	}
}

func TestRepository_PostAvailability(t *testing.T) {
	type Fields struct {
		StartDate string
		EndDate   string
	}
	tests := []struct {
		name     string
		fields   Fields
		withBody bool
		want     int
	}{
		{"invalid request",
			Fields{},
			false,
			http.StatusSeeOther},
		{"valid request",
			Fields{
				StartDate: "2050-12-23",
				EndDate:   "2050-12-24",
			},
			true,
			http.StatusOK},
		{"no room available",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "2050-01-02",
			},
			true,
			http.StatusSeeOther},
		{"invalid start date",
			Fields{
				StartDate: "invalid",
				EndDate:   "2050-01-02",
			},
			true,
			http.StatusSeeOther},
		{"invalid end date",
			Fields{
				StartDate: "2050-01-01",
				EndDate:   "invalid",
			},
			true,
			http.StatusSeeOther},
		{"search fails",
			Fields{
				StartDate: "2050-12-24",
				EndDate:   "2050-12-25",
			},
			true,
			http.StatusSeeOther},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			values := url.Values{}
			values.Add("start", tt.fields.StartDate)
			values.Add("end", tt.fields.EndDate)

			var req *http.Request
			if tt.withBody {
				req, _ = http.NewRequest("POST", "/search-availability", strings.NewReader(values.Encode()))
			} else {
				req, _ = http.NewRequest("POST", "/search-availability", nil)
			}
			ctx := getCtx(req)
			req = req.WithContext(ctx)

			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

			rr := httptest.NewRecorder()

			handler := http.HandlerFunc(Repo.PostAvailability)
			handler.ServeHTTP(rr, req)
			if rr.Code != tt.want {
				t.Errorf("%s returned wrong response code: got %d, wanted %d", tt.name, rr.Code, tt.want)
			}
		})
	}
}

var loginTests = []struct {
	name               string
	email              string
	expectedStatusCode int
	expectedHTML       string
	expectedLocation   string
}{
	{"valid credentials", "me@here.se", http.StatusSeeOther, "", "/"},
	{"invalid credentials", "you@here.se", http.StatusSeeOther, "", "/user/login"},
	{"invalid data", "j", http.StatusOK, `action="/user/login`, ""},
}

func TestLogin(t *testing.T) {
	for _, e := range loginTests {
		postedData := url.Values{}
		postedData.Add("email", e.email)
		postedData.Add("password", "test")

		req, _ := http.NewRequest("POST", "/user/login", strings.NewReader(postedData.Encode()))
		ctx := getCtx(req)
		req = req.WithContext(ctx)
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(Repo.PostShowLogin)
		handler.ServeHTTP(rr, req)
		if rr.Code != e.expectedStatusCode {
			t.Errorf("%s returned wrong response code: got %d, wanted %d", e.name, rr.Code, e.expectedStatusCode)
		}

		if e.expectedLocation != "" {
			actualLoc, _ := rr.Result().Location()
			if actualLoc.Path != e.expectedLocation {
				t.Errorf("%s returned wrong location: got %s, wanted %s", e.name, actualLoc, e.expectedLocation)
			}
		}

		if e.expectedHTML != "" {
			html := rr.Body.String()
			if !strings.Contains(html, e.expectedHTML) {
				t.Errorf("%s returned wrong html: did not find %s", e.name, e.expectedHTML)
			}
		}
	}
}

var adminPostShowReservationTests = []struct {
	name                 string
	url                  string
	postedData           url.Values
	expectedResponseCode int
	expectedLocation     string
	expectedHTML         string
}{
	{
		name: "valid-data-from-new",
		url:  "/admin/reservations/new/1/show",
		postedData: url.Values{
			"first_name": {"John"},
			"last_name":  {"Smith"},
			"email":      {"john@smith.com"},
			"phone":      {"555-555-5555"},
		},
		expectedResponseCode: http.StatusSeeOther,
		expectedLocation:     "/admin/reservations-new",
		expectedHTML:         "",
	},
	{
		name: "valid-data-from-all",
		url:  "/admin/reservations/all/1/show",
		postedData: url.Values{
			"first_name": {"John"},
			"last_name":  {"Smith"},
			"email":      {"john@smith.com"},
			"phone":      {"555-555-5555"},
		},
		expectedResponseCode: http.StatusSeeOther,
		expectedLocation:     "/admin/reservations-all",
		expectedHTML:         "",
	},
	{
		name: "valid-data-from-cal",
		url:  "/admin/reservations/cal/1/show",
		postedData: url.Values{
			"first_name": {"John"},
			"last_name":  {"Smith"},
			"email":      {"john@smith.com"},
			"phone":      {"555-555-5555"},
			"year":       {"2022"},
			"month":      {"01"},
		},
		expectedResponseCode: http.StatusSeeOther,
		expectedLocation:     "/admin/reservations-calendar?y=2022&m=01",
		expectedHTML:         "",
	},
}

// TestAdminPostShowReservation tests the AdminPostReservation handler
func TestAdminPostShowReservation(t *testing.T) {
	for _, e := range adminPostShowReservationTests {
		var req *http.Request
		if e.postedData != nil {
			req, _ = http.NewRequest("POST", "/user/login", strings.NewReader(e.postedData.Encode()))
		} else {
			req, _ = http.NewRequest("POST", "/user/login", nil)
		}
		ctx := getCtx(req)
		req = req.WithContext(ctx)
		req.RequestURI = e.url

		// set the header
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		rr := httptest.NewRecorder()

		// call the handler
		handler := http.HandlerFunc(Repo.AdminPostShowReservation)
		handler.ServeHTTP(rr, req)

		if rr.Code != e.expectedResponseCode {
			t.Errorf("failed %s: expected code %d, but got %d", e.name, e.expectedResponseCode, rr.Code)
		}

		if e.expectedLocation != "" {
			// get the URL from test
			actualLoc, _ := rr.Result().Location()
			if actualLoc.String() != e.expectedLocation {
				t.Errorf("failed %s: expected location %s, but got location %s", e.name, e.expectedLocation, actualLoc.String())
			}
		}

		// checking for expected values in HTML
		if e.expectedHTML != "" {
			// read the response body into a string
			html := rr.Body.String()
			if !strings.Contains(html, e.expectedHTML) {
				t.Errorf("failed %s: expected to find %s but did not", e.name, e.expectedHTML)
			}
		}
	}
}

var adminPostReservationCalendarTests = []struct {
	name                 string
	postedData           url.Values
	expectedResponseCode int
	expectedLocation     string
	expectedHTML         string
	blocks               int
	reservations         int
}{
	{
		name: "cal",
		postedData: url.Values{
			"year":  {time.Now().Format("2006")},
			"month": {time.Now().Format("01")},
			fmt.Sprintf("add_block_1_%s", time.Now().AddDate(0, 0, 2).Format("2006-01-2")): {"1"},
		},
		expectedResponseCode: http.StatusSeeOther,
	},
	{
		name:                 "cal-blocks",
		postedData:           url.Values{},
		expectedResponseCode: http.StatusSeeOther,
		blocks:               1,
	},
	{
		name:                 "cal-res",
		postedData:           url.Values{},
		expectedResponseCode: http.StatusSeeOther,
		reservations:         1,
	},
}

func TestPostReservationCalendar(t *testing.T) {
	for _, e := range adminPostReservationCalendarTests {
		var req *http.Request
		if e.postedData != nil {
			req, _ = http.NewRequest("POST", "/admin/reservations-calendar", strings.NewReader(e.postedData.Encode()))
		} else {
			req, _ = http.NewRequest("POST", "/admin/reservations-calendar", nil)
		}
		ctx := getCtx(req)
		req = req.WithContext(ctx)

		now := time.Now()
		bm := make(map[string]int)
		rm := make(map[string]int)

		currentYear, currentMonth, _ := now.Date()
		currentLocation := now.Location()

		firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
		lastOfMonth := firstOfMonth.AddDate(0, 1, -1)

		for d := firstOfMonth; d.After(lastOfMonth) == false; d = d.AddDate(0, 0, 1) {
			rm[d.Format("2006-01-2")] = 0
			bm[d.Format("2006-01-2")] = 0
		}

		if e.blocks > 0 {
			bm[firstOfMonth.Format("2006-01-2")] = e.blocks
		}

		if e.reservations > 0 {
			rm[lastOfMonth.Format("2006-01-2")] = e.reservations
		}

		session.Put(ctx, "block_map_1", bm)
		session.Put(ctx, "reservation_map_1", rm)

		// set the header
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		rr := httptest.NewRecorder()

		// call the handler
		handler := http.HandlerFunc(Repo.AdminPostReservationsCalendar)
		handler.ServeHTTP(rr, req)

		if rr.Code != e.expectedResponseCode {
			t.Errorf("failed %s: expected code %d, but got %d", e.name, e.expectedResponseCode, rr.Code)
		}

	}
}

var adminProcessReservationTests = []struct {
	name                 string
	queryParams          string
	expectedResponseCode int
	expectedLocation     string
}{
	{
		name:                 "process-reservation",
		queryParams:          "",
		expectedResponseCode: http.StatusSeeOther,
		expectedLocation:     "",
	},
	{
		name:                 "process-reservation-back-to-cal",
		queryParams:          "?y=2021&m=12",
		expectedResponseCode: http.StatusSeeOther,
		expectedLocation:     "",
	},
}

func TestAdminProcessReservation(t *testing.T) {
	for _, e := range adminProcessReservationTests {
		req, _ := http.NewRequest("GET", fmt.Sprintf("/admin/process-reservation/cal/1/do%s", e.queryParams), nil)
		ctx := getCtx(req)
		req = req.WithContext(ctx)

		rr := httptest.NewRecorder()

		handler := http.HandlerFunc(Repo.AdminProcessReservation)
		handler.ServeHTTP(rr, req)

		if rr.Code != http.StatusSeeOther {
			t.Errorf("failed %s: expected code %d, but got %d", e.name, e.expectedResponseCode, rr.Code)
		}
	}
}

var adminDeleteReservationTests = []struct {
	name                 string
	queryParams          string
	expectedResponseCode int
	expectedLocation     string
}{
	{
		name:                 "delete-reservation",
		queryParams:          "",
		expectedResponseCode: http.StatusSeeOther,
		expectedLocation:     "",
	},
	{
		name:                 "delete-reservation-back-to-cal",
		queryParams:          "?y=2021&m=12",
		expectedResponseCode: http.StatusSeeOther,
		expectedLocation:     "",
	},
}

func TestAdminDeleteReservation(t *testing.T) {
	for _, e := range adminDeleteReservationTests {
		req, _ := http.NewRequest("GET", fmt.Sprintf("/admin/process-reservation/cal/1/do%s", e.queryParams), nil)
		ctx := getCtx(req)
		req = req.WithContext(ctx)

		rr := httptest.NewRecorder()

		handler := http.HandlerFunc(Repo.AdminDeleteReservation)
		handler.ServeHTTP(rr, req)

		if rr.Code != http.StatusSeeOther {
			t.Errorf("failed %s: expected code %d, but got %d", e.name, e.expectedResponseCode, rr.Code)
		}
	}
}
