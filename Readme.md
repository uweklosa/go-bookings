# Bookings and Reservations

This is the repository for bookings and reservations project.

- Built in Go 1.16
- External dependencies
    - [chi router](https://github.com/go-chi/chi/v5)
    - [Alex Edwards SCS](https://github.com/alexedwards/scs/v2) session management
    - [nosurf](https://github.com/justinas/nosurf)
