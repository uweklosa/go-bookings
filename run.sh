#!/usr/bin/env bash

go build -o bookings cmd/web/*.go
./bookings -dbname=bookings -dbhost=192.168.8.102 -dbport=5433 -dbuser=bookings -dbpass=example -cache=false