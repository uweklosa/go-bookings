package forms

import (
	"fmt"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestForm_Valid(t *testing.T) {
	r := httptest.NewRequest("POST", "/whatever", nil)
	form := New(r.PostForm)

	if !form.Valid() {
		t.Error("got invalid form when it should have been valid")
	}
}

func TestForm_Required(t *testing.T) {
	r := httptest.NewRequest("POST", "/whatever", nil)
	form := New(r.PostForm)

	form.Required("a", "b", "c")
	if form.Valid() {
		t.Error("form show valid when required fields missing")
	}

	postedData := url.Values{}
	postedData.Add("a", "a")
	postedData.Add("b", "c")
	postedData.Add("c", "c")

	r = httptest.NewRequest("POST", "/whatever", nil)
	form = New(postedData)
	if !form.Valid() {
		t.Error("shows does not have required fields when it does")
	}
}

func TestForm_Has(t *testing.T) {
	r := httptest.NewRequest("POST", "/whatever", nil)
	form := New(r.PostForm)

	if form.Has("a") {
		t.Error("form shows it has a field when it does not")
	}

	postedData := url.Values{}
	postedData.Add("a", "a")

	r = httptest.NewRequest("POST", "/whatever", nil)
	form = New(postedData)
	if !form.Has("a") {
		t.Error("form shows it does not have a field when it does")
	}
}

func TestForm_IsEmail(t *testing.T) {
	tests := []struct {
		name  string
		field string
		value string
		error bool
	}{
		{name: "wrong field", field: "x", value: "test@testsson.com", error: true},
		{name: "empty string", field: "email", value: "", error: true},
		{name: "incorrect format", field: "email", value: "a@b", error: true},
		{name: "correct format", field: "email", value: "test@testsson.com", error: false},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			postedData := url.Values{}
			postedData.Add("email", test.value)
			form := New(postedData)
			form.IsEmail(test.field)
			if form.Valid() && test.error {
				t.Error(fmt.Sprintf("valid email %s identified as invalid email", test.value))
			}
			if !form.Valid() && !test.error {
				t.Error(fmt.Sprintf("invalid email %s identified as valid email", test.value))
			}
		})
	}
}

func TestForm_MinLength(t *testing.T) {
	tests := []struct {
		name   string
		field  string
		value  string
		length int
		error  bool
	}{
		{name: "non existent field", field: "x", value: "xxx", length: 2, error: true},
		{name: "empty string", field: "name", value: "", length: 2, error: true},
		{name: "string too short", field: "name", value: "a", length: 2, error: true},
		{name: "valid string", field: "name", value: "ab", length: 2, error: false},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			postedData := url.Values{}
			postedData.Add("name", test.value)
			form := New(postedData)
			form.MinLength(test.field, test.length)
			if form.Valid() && test.error {
				t.Error(fmt.Sprintf("valid field length %s identified as too short", test.value))
			}
			if !form.Valid() && !test.error {
				t.Error(fmt.Sprintf("invalid field length %s identified as ok", test.value))
			}
		})
	}
}
