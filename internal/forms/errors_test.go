package forms

import "testing"

func TestErrors_Add(t *testing.T) {
	e := errors{}
	e.Add("a", "message")
	if len(e) == 0 {
		t.Error("error not added")
	}
}

func TestErrors_Get(t *testing.T) {
	e := errors{}
	e.Add("a", "message")
	if e.Get("a") == "" {
		t.Error("error not added")
	}
	if e.Get("b") != "" {
		t.Error("found error that does not exist")
	}
}
